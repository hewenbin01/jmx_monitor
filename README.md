# jmx_monitor

 **使用jmx接口来监控tomcat和weblogic的当前线程数** 

可以作为独立的http server运行
1.通过eclipse导出 runnable jar file ，指定launch configution(jar的入口类，main方法所在地类)，指定jarname
2.java -jar **/jarname.jar 就可以启动了
3.需要监控的程序需要打开jmx接口，比如tomcat，修改 catalina.bat ，在 setlocal 后面加下面一段参数设置
```
set JMX_REMOTE_CONFIG=-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=8999 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false  
set CATALINA_OPTS=%CATALINA_OPTS% %JMX_REMOTE_CONFIG%
```

使用jmx接口来监控tomcat和weblogic的当前线程数

## 使用说明

* 在java环境中启动jar，jmx_monitor.jar在jmx_monitor目录下 

* 启动命令：java -jar jar路径/jmx_monitor.jar  

## 接口列表

1.获取weblogic当前线程数


* url: /jmx/weblogic/thread

* port：默认8888，如果需要修改，请设置环境变量“JMX_MONITOR_PORT”

* method：POST

* 响应类型：text/plain

* 响应码：成功：200,失败：500

* 请求示例：
	```
	[
		{
			"host":"127.0.0.1",
			"port":7001,
			"username":"weblogic",
			"password":"weblogic_123"
		},
		{
			"host":"126.0.0.1",
			"port":7001,
			"username":"weblogic",
			"password":"weblogic_123"
		}
	]
	
	参数说明:  
	host:weblogic服务器监控的IP地址  
	port:weblogic服务器监控的端口  
	username:weblogic控制台访问用户名  
	password:weblogic控制台访问密码  
	```
* 响应示例：

	成功：

	```
	[
		{
			"host":"127.0.0.1",
			"port":7001,
			"threadnum":7
		},
		{
			"host":"126.0.0.1",
			"port":7001,
			"threadnum":7
		}
	]  
	
	参数说明：    
	threadnum：当前线程数  
	```

	失败：""  

---

2.获取tomcat当前线程数


* url: /jmx/tomcat/thread

* method：POST

* 响应类型：text/plain

* 响应码：成功：200,失败：500

* 请求示例：
	```
	[
		{
			"host":"127.0.0.1",
			"port":8080,
		},
		{
			"host":"126.0.0.1",
			"port":8080,
		}
	]  
	参数说明:  
	host:tomcat服务器的IP地址    
	port:tomcat听过的JMX端口  
	```
	 
* 响应示例：

	成功：

	```
	[
		{
			"host":"127.0.0.1",
			"port":7001,
			"threadnum":7,
			"peakthreadnum":10
		},
		{
			"host":"126.0.0.1",
			"port":7001,
			"threadnum":7,
			"peakthreadnum":10
		}
	]  
	参数说明：  
	threadnum：当前线程数  
	peakthreadnum：线程数峰值  
	```
	 
	失败：""
