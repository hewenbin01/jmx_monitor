package cn.com.dnt.jmxmonitor.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * <p>项 目：dcos</p>
 * <p>模 块：json工具类</p>
 * <p>描 述：用于处理json数据</p>
 * <p>版 权: Copyright (c) 2016</p>
 * <p>公 司: 天玑科技</p>
 * @author jixiaogang
 * @version 1.0 
 * 创建时间：2016年9月1日 上午10:01:32
 */
public class JSONUtil {
	private final static ObjectMapper objectMapper = new ObjectMapper();
	
	static{
		//默认日期格式
		objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
	}
	
	private JSONUtil() {
		
	}
	
	public static ObjectMapper getInstance() {
		return objectMapper;
	}
	
	/**
	 * 对象转json字符串(对象必须是一个javaBean)
	 * @param obj
	 * @return json字符串
	 */
	public static String obj2json(Object obj){
		try {
			return objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * json字符串转对象
	 * @param jsonStr json字符串 
	 * @param clazz 对象class类型
	 * @return 对象
	 */
	public static <T> T json2obj(String jsonStr, Class<T> clazz) {
		try {
			return objectMapper.readValue(jsonStr, clazz);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * json字符串转map
	 * @param jsonStr
	 * @return map
	 */
	public static <T> Map<String, Object> json2map(String jsonStr) {
		try {
			return objectMapper.readValue(jsonStr, Map.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	/**
	 * map转json字符串
	 * @param map
	 * @return
	 */
	public static String map2json(Map<String, Object> map){
		try {
			return objectMapper.writeValueAsString(map);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * json字符串转map
	 * @param jsonStr
	 * @param 对象类型
	 * @return
	 */
	public static <T> Map<String, T> json2map(String jsonStr, Class<T> clazz) {
		Map<String, Map<String, Object>> map = null;
		try {
			map = objectMapper.readValue(jsonStr, new TypeReference<Map<String, T>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		Map<String, T> result = new HashMap<String, T>();
		for (Entry<String, Map<String, Object>> entry : map.entrySet()) {
			result.put(entry.getKey(), map2pojo(entry.getValue(), clazz));
		}
		return result;
	}

	/**
	 * json字符串转对象集合
	 * @param jsonArrayStr
	 * @param clazz
	 * @return
	 */
	public static <T> List<T> json2list(String jsonArrayStr, Class<T> clazz) {
		List<Map<String, Object>> list = null;
		try {
			list = objectMapper.readValue(jsonArrayStr, new TypeReference<List<T>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<T> result = new ArrayList<T>();
		for (Map<String, Object> map : list) {
			result.add(map2pojo(map, clazz));
		}
		return result;
	}

	/**
	 * map转对象
	 * @param map 
	 * @param clazz
	 * @return 对象
	 */
	public static <T> T map2pojo(Map map, Class<T> clazz) {
		return objectMapper.convertValue(map, clazz);
	}
	
}
 