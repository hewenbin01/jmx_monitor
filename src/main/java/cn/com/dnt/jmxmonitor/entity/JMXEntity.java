package cn.com.dnt.jmxmonitor.entity;

import java.io.Serializable;

public class JMXEntity implements Serializable{
	private static final long serialVersionUID = -8982522088096222638L;
	private String host;
	private int port;
	private String userName;
	private String passWord;
	private int threadnum;
	private int peakthreadnum;
	
	
	public String getUserName() {
		return userName;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getThreadnum() {
		return threadnum;
	}
	public void setThreadnum(int threadnum) {
		this.threadnum = threadnum;
	}
	public int getPeakthreadnum() {
		return peakthreadnum;
	}
	public void setPeakthreadnum(int peakthreadnum) {
		this.peakthreadnum = peakthreadnum;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "Entity [host=" + host + ", port=" + port + ", threadnum=" + threadnum + ", peakthreadnum="
				+ peakthreadnum + "]";
	}
	
}
