package cn.com.dnt.jmxmonitor.handler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import cn.com.dnt.jmxmonitor.handler.JVMThreadExtractor.ThreadInfo;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * 监控调用程序入口
 * @author hewenbin
 *
 */
public class JMXMonitor implements HttpHandler  {

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		String requestMethod = exchange.getRequestMethod();  
        if (requestMethod.equalsIgnoreCase("GET")) {  
            Headers responseHeaders = exchange.getResponseHeaders(); 
            String url = exchange.getRequestURI().getPath();
            url = url.substring(14);
            String[] host = url.split(":");
            String host_name = host[0];
            String port = host[1];
            int[] ports = {Integer.valueOf(port)};
            System.out.println("host_name:"+host_name);
            System.out.println("port:"+port);
            
            responseHeaders.set("Content-Type", "text/plain");  
  
//            String hostName = "localhost";
//    		int[] jmxPorts = {8999};
    		Map<String, Object> map = new HashMap<String, Object>(2,1);
    		int status = 200;
			try {
				map = getThreadNum(host_name, ports);
			} catch (Exception e) {
				e.printStackTrace();
				map.put("threadnum", 0);
				map.put("peakthreadnum", 0);
				status = 400;
			}
			exchange.sendResponseHeaders(status, 0);
			OutputStream responseBody = exchange.getResponseBody(); 
            String json = "{\"threadnum\":\""+map.get("threadnum")+"\",\"peakthreadnum\":\""+map.get("peakthreadnum")+"\"}";
            System.out.println("responsejson:"+json);
            System.out.println("status:"+status);
            responseBody.write(json.getBytes());  
            responseBody.close();  
        } 
	} 
	
	private static Map<String, Object> getThreadNum(String hostName,int[] jmxPorts ) throws Exception{
//		String hostName = "localhost";
//		int[] jmxPorts = {8999};
		Map<String, Object> map = new HashMap<String, Object>(2,1);
		for (int jmxPort :jmxPorts) {
			// 从JMX中获取JVM信息
			ProxyClient proxyClient = null;
			try {
				proxyClient = ProxyClient.getProxyClient(hostName, jmxPort, null, null);
				proxyClient.connect();
				
				JMXCall<ThreadInfo> threadExtractor = new JVMThreadExtractor(proxyClient, jmxPort);
				ThreadInfo threadInfo = threadExtractor.call();
//				System.out.println(threadInfo.getThreadNum());//当前线程数
//				System.out.println(threadInfo.getPeakThreadNum());//峰值
				map.put("threadnum", threadInfo.getThreadNum());
				map.put("peakthreadnum", threadInfo.getPeakThreadNum());
			} finally {
				if (proxyClient != null) {
					proxyClient.disconnect();
				}
			}
		}
			
		return map;
	}
	
}
