package cn.com.dnt.jmxmonitor.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class JMXCall<T> {

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	private final ProxyClient proxyClient;
	private final int jmxPort;
	
	public JMXCall(final ProxyClient proxyClient, int jmxPort) {
		this.proxyClient = proxyClient;
		
		if (jmxPort <= 0) {
			throw new IllegalStateException("jmxPort is 0, client=" + proxyClient.getUrl());
		}
		this.jmxPort = jmxPort;
	}
	
	/**
	 * 调用jmx接口获取数据
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract T call() throws Exception;
	

	/**
	 * @return the proxyClient
	 */
	public ProxyClient getProxyClient() {
		return proxyClient;
	}

	/**
	 * @return the jmxPort
	 */
	public int getJmxPort() {
		return jmxPort;
	}
}
