package cn.com.dnt.jmxmonitor.handler;

import java.io.IOException;

import cn.com.dnt.jmxmonitor.handler.JVMThreadExtractor.ThreadInfo;

public class JVMThreadExtractor extends JVMDataExtractor<ThreadInfo> {

	public JVMThreadExtractor(ProxyClient proxyClient, int jmxPort)
			throws IOException {
		super(proxyClient, jmxPort);
	}

	@Override
	public ThreadInfo call() throws Exception {
		int threadNum = getThreadMXBean().getThreadCount();
		int peakThreadNum = getThreadMXBean().getPeakThreadCount();
		ThreadInfo threadInfo = new ThreadInfo(threadNum, peakThreadNum);
		return threadInfo;
	}

	
	public class ThreadInfo {
		private final int threadNum;
		private final int peakThreadNum;
		public ThreadInfo(int threadNum, int peakThreadNum) {
			this.threadNum = threadNum;
			this.peakThreadNum = peakThreadNum;
		}
		
		/**
		 * @return the threadNum
		 */
		public int getThreadNum() {
			return threadNum;
		}
		
		/**
		 * @return the peakThreadNum
		 */
		public int getPeakThreadNum() {
			return peakThreadNum;
		}
	}
	
}
