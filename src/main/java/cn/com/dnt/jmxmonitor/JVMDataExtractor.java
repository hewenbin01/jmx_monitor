package cn.com.dnt.jmxmonitor;

import java.io.IOException;
import java.lang.management.ThreadMXBean;

public abstract class JVMDataExtractor<T> extends JMXCall<T> {
	
	
	private final ThreadMXBean threadMXBean;
	
	public JVMDataExtractor(ProxyClient proxyClient, int jmxPort) throws IOException { super(proxyClient, jmxPort);
		threadMXBean = proxyClient.getThreadMXBean();
	}

	/**
	 * @return the threadMXBean
	 */
	public ThreadMXBean getThreadMXBean() {
		return threadMXBean;
	}
	
}
