package cn.com.dnt.jmxmonitor.server;

import java.util.List;
import java.util.Map;

import cn.com.dnt.jmxmonitor.handler.JMXCall;
import cn.com.dnt.jmxmonitor.handler.JVMThreadExtractor;
import cn.com.dnt.jmxmonitor.handler.JVMThreadExtractor.ThreadInfo;
import cn.com.dnt.jmxmonitor.handler.ProxyClient;
import cn.com.dnt.jmxmonitor.util.JSONUtil;

/** 
 * <p>project：</p>
 * <p>module：tomcat线程监控</p>
 * <p>description：</p>
 * <p>company: 天玑科技</p>
 * <p>Copyright (c) 2016</p>
 * @author hewenbin
 * @version 1.0 
 * @date：2016年12月14日 下午3:30:58
 */
public class TomcatJMXService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<Map> getThreadNum(String reqJson) {
		List<Map> resList = JSONUtil.json2list(reqJson, Map.class);
		for (Map map : resList) {
			String host = map.get("host").toString();
			int jmxPort = Integer.valueOf(map.get("port").toString());
			// 从JMX中获取JVM信息
			ProxyClient proxyClient = null;
			try {
				proxyClient = ProxyClient.getProxyClient(host, jmxPort, null, null);
				proxyClient.connect();
				JMXCall<ThreadInfo> threadExtractor = new JVMThreadExtractor(proxyClient, jmxPort);
				ThreadInfo threadInfo = threadExtractor.call();
				map.put("threadnum",threadInfo.getThreadNum());
				map.put("peakthreadnum",threadInfo.getPeakThreadNum());
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			} finally {
				if (proxyClient != null) {
					proxyClient.disconnect();
				}
			}
		}
		return resList;
	}
}

