package cn.com.dnt.jmxmonitor.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import com.sun.net.httpserver.HttpServer;

import cn.com.dnt.jmxmonitor.handler.JMXMonitor;
/** 
 * <p>项 目：SOA</p>
 * <p>模 块：</p>
 * <p>描 述：</p>
 * <p>版 权: Copyright (c) 2016</p>
 * <p>公 司: 天玑科技</p>
 * @author hewenbin
 * @version 1.0 
 * 创建时间：2016年11月1日 下午5:33:39
 */
public class MyHttpServer {

	
	public static void main(String[] args) throws IOException {  
        InetSocketAddress addr = new InetSocketAddress(7777);  
        HttpServer server = HttpServer.create(addr, 0);  
        server.createContext("/thread", new JMXMonitor());  
        server.setExecutor(Executors.newCachedThreadPool());  
        server.start();
        System.out.println("Server is listening on port " + 7777);  
    }
	
}

