package cn.com.dnt.jmxmonitor.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import weblogic.management.*;
import cn.com.dnt.jmxmonitor.entity.JMXEntity;
import cn.com.dnt.jmxmonitor.server.JMXHttpServer;
import cn.com.dnt.jmxmonitor.server.WebLogicJMXService;
import cn.com.dnt.jmxmonitor.util.JSONUtil;

public class Local {
	public MBeanHome localHome;
	public MBeanHome adminHome;
	public static String SERVER_NAME = "AdminServer";

	public void find() {
		String url = "t3://127.0.0.1:7001";
		try {
			localHome = (MBeanHome) Helper.getMBeanHome("weblogic", "weblogic_123",url, SERVER_NAME);
			adminHome = (MBeanHome) Helper.getAdminMBeanHome("weblogic","weblogic_123", url);
			System.out.println("Local and Admin Homes  found using the Helper class");
		} catch (IllegalArgumentException iae) {
			System.out.println("Illegal Argument Exception: " + iae);
		}
	}

	public static void main2(String[] args) {
		Local test = new Local();
		test.find();
		Set allMBeans = test.localHome.getAllMBeans();
		System.out.println("Size: " + allMBeans.size());
		for (Iterator itr = allMBeans.iterator(); itr.hasNext();) {
			WebLogicMBean mbean = (WebLogicMBean) itr.next();
			System.out.println(mbean.getName());
//			WebLogicObjectName objectName = mbean.getObjectName();
//			System.out.println(objectName.getName() + " is a " + mbean.getType());
		}
//		Set all2MBeans = test.adminHome.getAllMBeans();
//		System.out.println("Size: " + all2MBeans.size());
//		for (Iterator itr = all2MBeans.iterator(); itr.hasNext();) {
//			WebLogicMBean mbean = (WebLogicMBean) itr.next();
//			WebLogicObjectName objectName = mbean.getObjectName();
//			System.out.println(objectName.getName() + "  isa "
//					+ mbean.getType());
//		}
	}

	public static void main(String[] args) {
//		JMXEntity entity = new JMXEntity();
//		entity.setHost("localhost");
//		entity.setPort(8999);
//		List<JMXEntity> entities = new ArrayList<>();
//		entities.add(entity);
//		String res = JMXHttpServer.getThreadNum(JSONUtil.obj2json(entities));
//		System.out.println("res:" + res);
		List<JMXEntity> resList = new ArrayList<JMXEntity>();
		for (int i = 0; i < 10; i++) {
			JMXEntity entity = new JMXEntity();
			entity.setUserName("weblogic");
			entity.setHost("127.0.0.1");
			entity.setPassWord("weblogic_123");
			entity.setPort(7001);
			resList.add(entity);
		}
		System.out.println(JSONUtil.obj2json(resList));
//		System.out.println( getThreadNum(JSONUtil.obj2json(resList)	));
	}
	
	public static String getThreadNum(String reqJson) {
		List<Map> resList = WebLogicJMXService.getThreadNum(reqJson);
//		List<JMXEntity> resList = TomcatJMXServer.getThreadNum(reqJson);
		return JSONUtil.obj2json(resList);
	}
}
